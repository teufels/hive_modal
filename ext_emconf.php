<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "hive_modal".
 *
 * Auto generated 18-08-2022 15:21
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
    'title' => 'HIVE>Modal',
    'description' => 'Modal / SlideOut / PopUp',
    'category' => 'fe',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'author_company' => 'teufels GmbH',
    'state' => 'stable',
    'version' => '2.2.2',
    'constraints' =>
        array (
            'depends' =>
                array (
                    'typo3' => '11.5.0-11.5.99',
                ),
            'conflicts' =>
                array (
                ),
            'suggests' =>
                array (
                ),
        ),
);

