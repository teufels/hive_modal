![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__modal-blue.svg)
![version](https://img.shields.io/badge/version-2.2.*-yellow.svg?style=flat-square)

HIVE > Modal
==========
Modal / SlideOut / PopUp

#### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_modal`

### Requirements
- `Bootstrap v5`

### How to use (Administrator)
- Install with composer
- Import Static Template (before hive_thm_custom)
- Styling / Position / Animation via CSS & Template Override

###  How to use (Integrator)
- place Hive>Modal (CE) where modal should appear
- could used with Media, Appearance>Background, Content elements

### Notice
- developed with mask & mask_export
- based on Bootstrap modal https://getbootstrap.com/docs/5.0/components/modal/

### Changelog
- 2.2.1 remove unused mask config
- 2.2.0 implement show after options
- 2.1.0 add possibility to use multiple modals on different sites
- 2.0.0 Typo3 v11 compatibility
- 1.0.0 stable
- 0.1.0 initial