CREATE TABLE tt_content (
    tx_hivemodal_backendtitle varchar(255) DEFAULT '' NOT NULL,
    tx_hivemodal_closeafter int(11) DEFAULT '0' NOT NULL,
    tx_hivemodal_closescrolling int(11) DEFAULT '0' NOT NULL,
    tx_hivemodal_closebackdrop int(11) DEFAULT '0' NOT NULL,
    tx_hivemodal_width int(11) DEFAULT '0' NOT NULL,
    tx_hivemodal_content int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivemodal_content_parent int(11) unsigned DEFAULT '0' NOT NULL,
    KEY tx_hivemodal_content_parent (tx_hivemodal_content_parent,pid,deleted),
    tx_hivemodal_link varchar(255) DEFAULT '' NOT NULL,
    tx_hivemodal_media int(11) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tt_content (
    tx_hivemodal_closeafter_active tinyint(1) DEFAULT '0' NOT NULL,
    tx_hivemodal_openafter_active tinyint(1) DEFAULT '0' NOT NULL,
    tx_hivemodal_openafter int(11) DEFAULT '0' NOT NULL,
    tx_hivemodal_openscrolling int(11) DEFAULT '0' NOT NULL,
);
